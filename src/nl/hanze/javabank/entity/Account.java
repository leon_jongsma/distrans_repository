package nl.hanze.javabank.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="account")
public class Account  implements Serializable
{
	 /** Default value included to remove warning. Remove or modify at will. **/
    private static final long serialVersionUID = 1L;

	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	private String naam;
	private String adres;
	private String woonplaats;
	private Integer rekeningNummer;
	private Double saldo;

	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public String getAdres() {
		return adres;
	}
	public void setAdres(String adres) {
		this.adres = adres;
	}
	public String getWoonplaats() {
		return woonplaats;
	}
	public void setWoonplaats(String woonplaats) {
		this.woonplaats = woonplaats;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Integer getRekeningNummer() {
		return rekeningNummer;
	}
	public void setRekeningNummer(Integer rekeningNummer) {
		this.rekeningNummer = rekeningNummer;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", naam=" + naam + ", adres=" + adres
				+ ", woonplaats=" + woonplaats + ", rekeningNummer="
				+ rekeningNummer + ", saldo=" + saldo + "]";
	}
	


}
