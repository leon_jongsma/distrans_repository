package nl.hanze.cjibbank.bean;

import java.util.Date;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import nl.hanze.javabank.entity.Account;

public class TransactionBean 
{
	private Long id;
	private Account sender;
	private Account receiver;	
	private Integer amount;
	private Date date;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Account getSender() {
		return sender;
	}
	public void setSender(Account sender) {
		this.sender = sender;
	}
	public Account getReceiver() {
		return receiver;
	}
	public void setReceiver(Account receiver) {
		this.receiver = receiver;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	


}
