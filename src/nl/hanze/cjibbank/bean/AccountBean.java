package nl.hanze.cjibbank.bean;

public class AccountBean 
{
	private Long id;	
	private String naam;
	private String adres;
	private String woonplaats;
	private Integer rekeningNummer;
	private Double saldo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public String getAdres() {
		return adres;
	}
	public void setAdres(String adres) {
		this.adres = adres;
	}
	public String getWoonplaats() {
		return woonplaats;
	}
	public void setWoonplaats(String woonplaats) {
		this.woonplaats = woonplaats;
	}
	public Integer getRekeningNummer() {
		return rekeningNummer;
	}
	public void setRekeningNummer(Integer rekeningNummer) {
		this.rekeningNummer = rekeningNummer;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	

}
