package nl.hanze.cjibbank.bean;

public class TransactionDto 
{
	Integer amount; 
	Integer reknummerFrom; 
	Integer reknummerTo;
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getReknummerFrom() {
		return reknummerFrom;
	}
	public void setReknummerFrom(Integer reknummerFrom) {
		this.reknummerFrom = reknummerFrom;
	}
	public Integer getReknummerTo() {
		return reknummerTo;
	}
	public void setReknummerTo(Integer reknummerTo) {
		this.reknummerTo = reknummerTo;
	}
	
	

}
