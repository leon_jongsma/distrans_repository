package nl.hanze.distrans.sessionbean;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import nl.hanze.cjibbank.bean.TransactionDto;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ClearingHouseEjb implements ClearingHouseEjbLocal
{
	 @PersistenceContext(unitName = "javabank_pu_xa")
	 private EntityManager javaBankEntityManager;
	 @PersistenceContext(unitName = "cjibbank_pu_xa")
	 private EntityManager cjibBankEntityManager;
	 
	@Override
	public boolean transferFromJavaBankToCjibBank(TransactionDto transactionDto)
	{
		try 
		{
			
			nl.hanze.javabank.entity.Account from = getJavaBankAccount(transactionDto.getReknummerFrom());
			nl.hanze.cjibbank.entity.Account to = getCjibBankAccount(transactionDto.getReknummerTo());			
			deductMoneyFromJavaBank(from,transactionDto.getAmount());
			addMoneyToCjibBank(to,transactionDto.getAmount());			
		}
		catch (Exception e) 
		{
			return false;
		} 
		return true;
	}
	
	@Override
	public boolean transferFromCjibBankToJavaBank(TransactionDto transactionDto) 
	{
		try 
		{
			nl.hanze.cjibbank.entity.Account from = getCjibBankAccount(transactionDto.getReknummerFrom());
			nl.hanze.javabank.entity.Account to = getJavaBankAccount(transactionDto.getReknummerTo());		
			deductMoneyFromCjibBank(from,transactionDto.getAmount());
			addMoneyToJavaBank(to,transactionDto.getAmount());			
		}
		catch (Exception e) 
		{
			return false;
		} 

		return true;
	}


	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void addMoneyToJavaBank(nl.hanze.javabank.entity.Account to,Integer amount) throws Exception 
	{
		if (to.getSaldo() > 0 && to.getSaldo().intValue() + amount.intValue() > 0)
		{
			int saldo = to.getSaldo().intValue() + amount.intValue();
			to.setSaldo(new Double(saldo));
			updateJavaBankAccount(to);	
		}
		else
		{
			throw new Exception();
		}			
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void deductMoneyFromCjibBank(nl.hanze.cjibbank.entity.Account from, Integer amount) throws Exception
	{
		if (from.getSaldo() > 0 && from.getSaldo().intValue() - amount.intValue() > 0)
		{
			int saldo = from.getSaldo().intValue() - amount.intValue();
			from.setSaldo(new Double(saldo));
			updateCjibBankAccount(from);	
		}
		else
		{
			throw new Exception();
		}		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void addMoneyToCjibBank(nl.hanze.cjibbank.entity.Account to, Integer amount)  throws Exception 
	{
		if (to.getSaldo() > 0 && to.getSaldo().intValue() + amount.intValue() > 0)
		{
			int saldo = to.getSaldo().intValue() + amount.intValue();
			to.setSaldo(new Double(saldo));
			updateCjibBankAccount(to);	
		}
		else
		{
			throw new Exception();
		}		
	}



    @TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deductMoneyFromJavaBank(nl.hanze.javabank.entity.Account account,Integer amount) throws Exception 
	{
		System.out.println("incoming");
		if (account.getSaldo() > 0 && account.getSaldo().intValue() - amount.intValue() > 0)
		{
			int saldo = account.getSaldo().intValue() - amount.intValue();
			account.setSaldo(new Double(saldo));
			updateJavaBankAccount(account);	
		}
		else
		{
			throw new Exception();
		}
	}
	
	
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void updateJavaBankAccount(nl.hanze.javabank.entity.Account account) 
	{
		javaBankEntityManager.merge(account);		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void updateCjibBankAccount(nl.hanze.cjibbank.entity.Account account) 
	{
		cjibBankEntityManager.merge(account);		
	}


	
	
	
	public nl.hanze.javabank.entity.Account getJavaBankAccount(Integer reknummer) throws Exception
	{
		Query queryAccountByAccountNumber = javaBankEntityManager.createQuery("SELECT OBJECT(a) FROM nl.hanze.javabank.entity.Account a WHERE a.rekeningNummer = :rekeningNummer");
		queryAccountByAccountNumber.setParameter("rekeningNummer", reknummer);		
		System.out.println("rekeningnummer: " + queryAccountByAccountNumber.getMaxResults());
		System.out.println(queryAccountByAccountNumber.getSingleResult().toString());
		if (queryAccountByAccountNumber.getMaxResults() > 0)
		{
			return (nl.hanze.javabank.entity.Account) queryAccountByAccountNumber.getSingleResult();
		}
		throw new Exception();
	}
	
	//@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public nl.hanze.cjibbank.entity.Account getCjibBankAccount(Integer reknummer) throws Exception
	{
		Query queryAccountByAccountNumber = cjibBankEntityManager.createQuery("SELECT OBJECT(a) FROM nl.hanze.cjibbank.entity.Account a WHERE a.rekeningNummer = :rekeningNummer");
		queryAccountByAccountNumber.setParameter("rekeningNummer", reknummer);		
		System.out.println(queryAccountByAccountNumber.getMaxResults());

		if (queryAccountByAccountNumber.getMaxResults() > 0)
		{
			return (nl.hanze.cjibbank.entity.Account) queryAccountByAccountNumber.getSingleResult();
		}
		throw new Exception();
	}
	
}
