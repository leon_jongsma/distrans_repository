package nl.hanze.distrans.sessionbean;

import javax.ejb.Local;

import nl.hanze.cjibbank.bean.AccountBean;
import nl.hanze.cjibbank.bean.TransactionBean;
import nl.hanze.cjibbank.bean.TransactionDto;

@Local
public interface ClearingHouseEjbLocal 
{
	public boolean transferFromJavaBankToCjibBank(TransactionDto transactionDto);
	public boolean transferFromCjibBankToJavaBank(TransactionDto transactionDto); 
}
