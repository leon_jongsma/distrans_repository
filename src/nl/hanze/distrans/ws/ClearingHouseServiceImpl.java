package nl.hanze.distrans.ws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import nl.hanze.cjibbank.bean.TransactionDto;
import nl.hanze.distrans.sessionbean.ClearingHouseEjbLocal;

@WebService(serviceName = "DisTransWS")
@Stateless()
public class ClearingHouseServiceImpl implements ClearingHouseService
{
	 @EJB
	 private ClearingHouseEjbLocal clearingHouse;

	@Override
	public boolean transferFromJavaBankToCjibBank(TransactionDto transactionDto) 
	{
		return clearingHouse.transferFromJavaBankToCjibBank(transactionDto);
	}

	@Override
	public boolean transferFromCjibBankToJavaBank(TransactionDto transactionDto) 
	{
		return clearingHouse.transferFromCjibBankToJavaBank(transactionDto); 
	}

	
	 
	
}
