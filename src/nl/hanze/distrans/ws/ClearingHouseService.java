package nl.hanze.distrans.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;

import nl.hanze.cjibbank.bean.TransactionDto;

public interface ClearingHouseService 
{
	public boolean transferFromJavaBankToCjibBank(TransactionDto transactionDto);
	public boolean transferFromCjibBankToJavaBank(TransactionDto transactionDto);
		
	
	
}
